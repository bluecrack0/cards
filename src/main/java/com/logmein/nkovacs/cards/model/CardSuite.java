package com.logmein.nkovacs.cards.model;

public enum CardSuite {
    HEART, SPADE, CLUB, DIAMOND,
}
