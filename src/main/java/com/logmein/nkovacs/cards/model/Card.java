package com.logmein.nkovacs.cards.model;

import java.util.Objects;

public class Card {

    private CardSuite suite;
    private int number;
    private CardType cardType;

    public Card() { }

    private Card(CardBuilder builder) {
        this.suite = builder.suite;
        this.number = builder.number;
        this.cardType = builder.cardType;
    }

    public int getNumber() {
        return number;
    }

    public CardSuite getSuite() {
        return suite;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Card card = (Card) o;
        return number == card.number &&
                suite == card.suite &&
                cardType == card.cardType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(suite, number, cardType);
    }

    public static class CardBuilder {
        private CardSuite suite;
        private int number;
        private CardType cardType;

        public CardBuilder() {}

        public CardBuilder suite(CardSuite suite) {
            this.suite = suite;
            return this;
        }

        public CardBuilder number(int number) {
            this.number = number;
            return this;
        }

        public CardBuilder cardType(CardType cardType) {
            this.cardType = cardType;
            return this;
        }

        public Card build() {
            return new Card(this);
        }
    }
}
