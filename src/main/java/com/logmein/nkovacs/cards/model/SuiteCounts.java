package com.logmein.nkovacs.cards.model;

public class SuiteCounts {
    private CardSuite cardSuite;
    private int leftCount;

    public SuiteCounts(CardSuite cardSuite, int leftCount) {
        this.cardSuite = cardSuite;
        this.leftCount = leftCount;
    }

    public CardSuite getCardSuite() {
        return cardSuite;
    }

    public int getLeftCount() {
        return leftCount;
    }
}
