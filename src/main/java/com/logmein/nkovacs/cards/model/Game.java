package com.logmein.nkovacs.cards.model;

import java.util.*;
import java.util.stream.Collectors;

public class Game {
    private Set<Player> players;
    private List<Deck> decks;
    private String gameId;
    private int lastPlayerCount;

    public Game() {
        this.players = new HashSet<>();
        this.decks = new ArrayList<>();
        this.lastPlayerCount = 0;
    }

    public Game(Set<Player> players, List<Deck> decks) {
        this.players = players;
        this.decks = decks;
        this.lastPlayerCount = players.size()+1;
    }

    public boolean removePlayer(Player player) {
        if (player == null || player.getPlayerId() == null) {
            return false;
        }
        return players.removeIf(player::equals);
    }

    public Player addPlayer(Player player) {
        if (player == null || player.getPlayerId() == null) {
            return null;
        }

        Player prevPlayer = players.stream()
                .filter(p -> p.getPlayerId().equals(player.getPlayerId()))
                .findAny()
                .orElse(null);

        if (prevPlayer == null) {
            player.setPlayerId("PLAYER_" + lastPlayerCount);
            ++lastPlayerCount;
        }
        return player;
    }

    public Set<Player> getPlayers() {
        return players;
    }

    public void setPlayers(Set<Player> players) {
        this.players = players;
    }

    public List<Deck> getDecks() {
        return decks;
    }

    public void setDecks(List<Deck> decks) {
        this.decks = decks;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public String getGameId() {
        return gameId;
    }

    public List<Card> getGameDeck() {
        return getDecks().stream()
                .map(Deck::getDeckCards)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    public boolean removeFromGameDeck(Card card) {
        boolean removed = false;
        for (int i = 0; i < decks.size() && !removed; ++i) {
            Deck d = decks.get(i);
            removed = d.getDeckCards().remove(card);
        }

        return removed;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Game game = (Game) o;
        return Objects.equals(gameId, game.gameId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(gameId);
    }
}
