package com.logmein.nkovacs.cards.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Player {
    private String playerId;
    private List<Card> playerCards = new ArrayList<>();

    public String getPlayerId() {
        return playerId;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }

    public List<Card> getPlayerCards() {
        return playerCards;
    }

    public void setPlayerCards(List<Card> playerCards) {
        this.playerCards = playerCards;
    }

    public int getCardSum() {
        return playerCards.stream()
                .mapToInt(Card::getNumber)
                .sum();
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Player player = (Player) o;
        return Objects.equals(playerId, player.playerId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(playerId);
    }
}
