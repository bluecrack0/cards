package com.logmein.nkovacs.cards.model;

import java.util.*;

public class Deck {
    private List<Card> deckCards = new LinkedList<>();

    public List<Card> getDeckCards() {
        return deckCards;
    }
}
