package com.logmein.nkovacs.cards.model;

public enum CardType {
    ACE, NUMBER, JACK, QUEEN, KING,
}
