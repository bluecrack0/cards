package com.logmein.nkovacs.cards;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan(basePackages = {"com.logmein.nkovacs.cards"})
public class CardsApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(CardsApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception { }
}
