package com.logmein.nkovacs.cards.repository;

import com.logmein.nkovacs.cards.model.Game;
import com.logmein.nkovacs.cards.model.Player;

public interface GameRepository {
    Game saveOrUpdate(Game game);
    Game get(Game game);
    Game getById(String id);
    Player getPlayerById(String id);
    boolean deleteGame(String id);
}
