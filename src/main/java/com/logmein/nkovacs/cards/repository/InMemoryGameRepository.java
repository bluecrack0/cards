package com.logmein.nkovacs.cards.repository;

import com.logmein.nkovacs.cards.model.Game;
import com.logmein.nkovacs.cards.model.Player;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class InMemoryGameRepository implements GameRepository {
    private static int currentId = 0;
    private Set<Game> gameCache = new HashSet<>();

    @Override
    public Game saveOrUpdate(Game game) {
        if (game == null) {
            return null;
        }

        Game existing = getById(game.getGameId());

        if (existing != null) {
            deleteGame(existing.getGameId());
            game.setGameId(existing.getGameId());
        } else {
            game.setGameId(generateId(game));
        }

        gameCache.add(game);

        return game;
    }

    private String generateId(Game game) {
        ++currentId;
        return "GAME_"+currentId;
    }

    @Override
    public Game get(Game game) {
        if (game == null || game.getGameId() == null) {
            return null;
        }
        return getById(game.getGameId());
    }

    @Override
    public Game getById(String id) {
        if (id == null) {
            return null;
        }

        Optional<Game> gameOption = gameCache.stream()
                .filter(g -> id.equals(g.getGameId()))
                .findFirst();

        return gameOption.orElse(null);
    }

    @Override
    public Player getPlayerById(String id) {
        return gameCache
                .stream()
                .map(Game::getPlayers)
                .flatMap(Collection::stream)
                .filter(p -> p.getPlayerId().equals(id))
                .findFirst()
                .orElse(null);
    }

    @Override
    public boolean deleteGame(String id) {
        return gameCache.removeIf(g -> g.getGameId().equals(id));
    }
}
