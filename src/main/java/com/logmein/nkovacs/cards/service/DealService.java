package com.logmein.nkovacs.cards.service;

import com.logmein.nkovacs.cards.model.Card;
import com.logmein.nkovacs.cards.model.Deck;
import com.logmein.nkovacs.cards.model.Game;
import com.logmein.nkovacs.cards.model.Player;
import com.logmein.nkovacs.cards.repository.GameRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class DealService {

    private GameRepository gameRepository;
    private static final Logger LOGGER = LoggerFactory.getLogger(DealService.class);
    private Random r = new Random();

    @Autowired
    public DealService(GameRepository gameRepository) {
        this.gameRepository = gameRepository;
    }

    public void dealCard(String gameId, String playerId, int count) {
        Game game = gameRepository.getById(gameId);

        if (game == null || game.getPlayers() == null) {
            LOGGER.error("Error dealing card to player with id: {} because game with id: {} doesnt have players or doesnt exist.",
                    playerId, gameId);
            return;
        }

        Player player = game.getPlayers()
                .stream()
                .filter(p -> p.getPlayerId().equals(playerId))
                .findFirst()
                .orElse(null);

        if (player == null) {
            LOGGER.error("Error dealing card to player with id: {} because it doesnt exist", playerId);
            return;
        }

        for (int i = 0; i < count; ++i) {
            Card c = getRandomCardFromGameDeck(game).orElse(null);

            if (c == null) {
                LOGGER.error("Error dealing card to player with id: {} because game deck is out of cards", playerId);
                break;
            } else {
                player.getPlayerCards().add(c);
            }
        }

        game.getPlayers().remove(player);
        game.getPlayers().add(player);
        gameRepository.saveOrUpdate(game);
    }

    private Optional<Card> getRandomCardFromGameDeck(Game game) {
        if (game == null) {
            return Optional.empty();
        }

        if (game.getGameDeck() == null) {
            return Optional.empty();
        }

        if (game.getGameDeck().size() == 0) {
            return Optional.empty();
        }

        List<Card> gameDeck = game.getGameDeck();

        Card randomCard = gameDeck.get(r.nextInt(gameDeck.size()));
        game.removeFromGameDeck(randomCard);

        Game updateGameOption = gameRepository.saveOrUpdate(game);

        return updateGameOption != null ? Optional.of(randomCard) : Optional.empty();
    }

    public void shuffleShoe(String gameId) {
        Game game = gameRepository.getById(gameId);

        if (game == null) {
            return;
        }

        List<Deck> decks = game.getDecks();

        for (int i = 0; i < decks.size(); ++i) {
            Card[] cardArray = decks.get(i)
                    .getDeckCards()
                    .toArray(new Card[]{});

            for (int j = 0; j < cardArray.length; ++j) {
                int index1 = (int) (Math.random() * cardArray.length);
                int index2 = (int) (Math.random() * cardArray.length);

                change(index1, index2, cardArray);
            }

            decks.get(i).getDeckCards().clear();
            decks.get(i).getDeckCards().addAll(Arrays.asList(cardArray));
        }
        gameRepository.saveOrUpdate(game);
    }

    private static void change(int index1, int index2, Card[] arr) {
        Card temp = arr[index1];

        arr[index1] = arr[index2];
        arr[index2] = temp;
    }
}
