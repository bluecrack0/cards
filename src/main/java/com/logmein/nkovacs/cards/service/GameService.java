package com.logmein.nkovacs.cards.service;

import com.logmein.nkovacs.cards.model.Deck;
import com.logmein.nkovacs.cards.model.Game;
import com.logmein.nkovacs.cards.model.Player;
import com.logmein.nkovacs.cards.repository.GameRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class GameService {

    private GameRepository gameRepository;
    private static final Logger LOGGER = LoggerFactory.getLogger(GameService.class);
    private Map<Game, Integer> gamePlayerMaxCount = new HashMap<>();

    @Autowired
    public GameService(GameRepository gameRepository) {
        this.gameRepository = gameRepository;
    }

    public Optional<Game> addDeckToGame(String id, Deck deck) {
        Game game = getGameById(id).
                orElse(null);

        if (game == null) {
            return Optional.empty();
        }

        game.getDecks().add(deck);
        return saveOrUpdateGame(game);
    }

    public Optional<Game> addPlayer(String gameId) {
        Player player = new Player();
        return addPlayerToGame(gameId, player);
    }

    public Optional<Game> addPlayerToGame(String gameId, Player player) {
        Game game = getGameById(gameId)
                .orElse(null);

        if (game == null || player == null) {
            return Optional.empty();
        }

        Integer playerId = getGamePlayerMaxCount().get(game);

        if (playerId == null) {
            getGamePlayerMaxCount().put(game, 1);
            player.setPlayerId("PLAYER_1");
            game.getPlayers().add(player);
            return saveOrUpdateGame(game);
        } else {
            int newId = playerId+1;
            getGamePlayerMaxCount().put(game, newId);
            player.setPlayerId("PLAYER_"+newId);
            game.getPlayers().add(player);
            return saveOrUpdateGame(game);
        }
    }

    public Optional<Game> removePlayerFromGame(String gameId, String playerId) {
        if (gameId == null || playerId == null) {
            LOGGER.error("Null received as gameId or PlayerId when removing player.");
            return Optional.empty();
        }

        Game game = getGameById(gameId)
                .orElse(null);

        if (game == null) {
            return Optional.empty();
        }

        game.getPlayers().removeIf(p -> p.getPlayerId().equals(playerId));

        return saveOrUpdateGame(game);
    }

    public Optional<Game> createGame() {
        Game game = new Game();
        game.setDecks(new ArrayList<>());
        game.setPlayers(new HashSet<>());
        return saveOrUpdateGame(game);
    }

    private Map<Game, Integer> getGamePlayerMaxCount() {
        return gamePlayerMaxCount;
    }

    public Optional<Game> getGameById(String id) {
        if (id == null) {
            LOGGER.error("Null received as Game ID when trying to fetch game by object.");
            return Optional.empty();
        }
        return Optional.ofNullable(gameRepository.getById(id));
    }

    public boolean deleteGame(String id) {
        if (id == null) {
            return false;
        }

        return gameRepository.deleteGame(id);
    }

    private Optional<Game> saveOrUpdateGame(Game game) {
        return Optional.ofNullable(this.gameRepository.saveOrUpdate(game));
    }
}
