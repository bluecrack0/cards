package com.logmein.nkovacs.cards.service;

import com.logmein.nkovacs.cards.model.*;
import com.logmein.nkovacs.cards.repository.GameRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class CardService {
    private GameRepository gameRepository;

    @Autowired
    public CardService(GameRepository gameRepository) {
        this.gameRepository = gameRepository;
    }

    public Optional<List<Card>> getCardForPlayers(String playerId) {
        Player player = gameRepository.getPlayerById(playerId);

        if (player == null) {
            return Optional.empty();
        }

        return Optional.of(player.getPlayerCards());
    }

    public Optional<List<Player>> getPlayersCardOrderDescending(String gameId) {
        Game g = gameRepository.getById(gameId);

        if (g == null) {
            return Optional.empty();
        }

        return Optional.of(
                g.getPlayers().stream()
                        .sorted(Comparator.comparingInt(Player::getCardSum).reversed())
                        .collect(Collectors.toList())
        );
    }

    public Optional<List<SuiteCounts>> getRemainingSuits(String gameId) {
        Game g = gameRepository.getById(gameId);

        if (g == null) {
            return Optional.empty();
        }

        Map<CardSuite, Long> suiteCount = g.getDecks().stream()
                .map(Deck::getDeckCards)
                .flatMap(Collection::stream)
                .collect(Collectors.groupingBy(Card::getSuite, Collectors.counting()));

        if (suiteCount.isEmpty()) {
            return Optional.empty();
        }

        return Optional.of(
                suiteCount.entrySet()
                        .stream()
                        .map(e -> new SuiteCounts(e.getKey(), e.getValue().intValue()))
                        .collect(Collectors.toList())
        );
    }

    public Optional<List<Card>> getRemainingCards(String gameId) {
        Game g = gameRepository.getById(gameId);

        if (g == null) {
            return Optional.empty();
        }

        Comparator<Card> suiteComparator = Comparator.comparing(c -> c.getSuite().name());

        return Optional.of(
                g.getDecks()
                        .stream()
                        .map(Deck::getDeckCards)
                        .flatMap(Collection::stream)
                        .sorted(suiteComparator.reversed()
                                .thenComparing(Card::getNumber).reversed()
                        )
                        .collect(Collectors.toList())
        );

    }
}
