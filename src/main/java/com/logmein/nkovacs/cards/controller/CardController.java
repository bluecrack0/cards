package com.logmein.nkovacs.cards.controller;

import com.logmein.nkovacs.cards.model.Card;
import com.logmein.nkovacs.cards.model.Player;
import com.logmein.nkovacs.cards.model.SuiteCounts;
import com.logmein.nkovacs.cards.service.CardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/gameAPI")
public class CardController {
    private CardService cardService;

    @Autowired
    public CardController(CardService cardService) {
        this.cardService = cardService;
    }

    @RequestMapping(value = "/getCardsForPlayer/{playerId}", method = RequestMethod.GET)
    public ResponseEntity<List<Card>> getCardsForPlayer(@PathVariable String playerId) {
        Optional<List<Card>> cardsOption = cardService.getCardForPlayers(playerId);
        if (cardsOption.isPresent()) {
            return ResponseEntity.ok(cardsOption.get());
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(value = "/getPlayersCardOrderDescending/{gameId}", method = RequestMethod.GET)
    public ResponseEntity<List<Player>> getPlayersCardOrderDescending(@PathVariable String gameId) {
        Optional<List<Player>> playerOption = cardService.getPlayersCardOrderDescending(gameId);
        if (playerOption.isPresent()) {
            return ResponseEntity.ok(playerOption.get());
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(value = "/getRemainingSuits/{gameId}", method = RequestMethod.GET)
    public ResponseEntity<List<SuiteCounts>> getRemainingSuits(@PathVariable String gameId) {
        Optional<List<SuiteCounts>> suiteOption = cardService.getRemainingSuits(gameId);
        if (suiteOption.isPresent()) {
            return ResponseEntity.ok(suiteOption.get());
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(value = "/getRemainingCards/{gameId}", method = RequestMethod.GET)
    public ResponseEntity<List<Card>> getRemainingCards(@PathVariable String gameId) {
        Optional<List<Card>> cardOption = cardService.getRemainingCards(gameId);
        if (cardOption.isPresent()) {
            return ResponseEntity.ok(cardOption.get());
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
