package com.logmein.nkovacs.cards.controller;

import com.logmein.nkovacs.cards.service.DealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/gameAPI")
public class DealController {

    private DealService dealService;

    @Autowired
    public DealController(DealService dealService) {
        this.dealService = dealService;
    }

    @RequestMapping(value = "/shuffleShoe/{gameId}", method = RequestMethod.POST)
    public ResponseEntity shuffleShoe(@PathVariable String gameId) {
        dealService.shuffleShoe(gameId);
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/dealCard/{gameId}/{playerId}", method = RequestMethod.POST)
    public ResponseEntity dealCard(@PathVariable String gameId, @PathVariable String playerId,
                                      @RequestParam int count) {
        dealService.dealCard(gameId, playerId, count);
        return ResponseEntity.ok().build();
    }

}
