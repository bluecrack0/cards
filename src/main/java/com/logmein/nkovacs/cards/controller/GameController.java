package com.logmein.nkovacs.cards.controller;

import com.logmein.nkovacs.cards.model.Deck;
import com.logmein.nkovacs.cards.model.Game;
import com.logmein.nkovacs.cards.model.Player;
import com.logmein.nkovacs.cards.service.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/gameAPI")
public class GameController {

    private GameService gameService;

    @Autowired
    public GameController(GameService gameService) {
        this.gameService = gameService;
    }

    @RequestMapping(value = "/createNewGame", method = RequestMethod.POST)
    public ResponseEntity<Game> createEmptyGame() {
        Optional<Game> gameOption = gameService.createGame();
        if (gameOption.isPresent()) {
            return ResponseEntity.ok(gameOption.get());
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(value = "/addDeckToGame/{gameId}", method = RequestMethod.POST)
    public ResponseEntity<Game> addDeckToGame(@PathVariable String gameId, @RequestBody Deck deck) {
        Optional<Game> gameOption = gameService.addDeckToGame(gameId, deck);
        if (gameOption.isPresent()) {
            return ResponseEntity.ok(gameOption.get());
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(value = "/addPlayerToGame/{gameId}", method = RequestMethod.POST)
    public ResponseEntity<Game> addPlayerToGame(@PathVariable String gameId, @RequestBody Player player) {
        Optional<Game> gameOption = gameService.addPlayerToGame(gameId, player);
        if (gameOption.isPresent()) {
            return ResponseEntity.ok(gameOption.get());
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(value = "/addEmptyPlayerToGame/{gameId}", method = RequestMethod.POST)
    public ResponseEntity<Game> addEmptyPlayerToGame(@PathVariable String gameId) {
        Optional<Game> gameOption = gameService.addPlayer(gameId);
        if (gameOption.isPresent()) {
            return ResponseEntity.ok(gameOption.get());
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(value = "/removePlayerFromGame/{gameId}/{playerId}", method = RequestMethod.POST)
    public ResponseEntity<Game> removePlayerFromGame(@PathVariable String gameId, @PathVariable String playerId) {
        Optional<Game> gameOption = gameService.removePlayerFromGame(gameId, playerId);
        if (gameOption.isPresent()) {
            return ResponseEntity.ok(gameOption.get());
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(value = "/deleteGame/{gameId}", method = RequestMethod.POST)
    public ResponseEntity deleteGame(@PathVariable String gameId) {
        if (gameService.deleteGame(gameId)) {
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
