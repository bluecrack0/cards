package com.logmein.nkovacs.cards.util;

import com.logmein.nkovacs.cards.model.*;

import java.util.Arrays;

public class DummyDataBuilder {

    public static Card buildCard(int number, CardSuite cardSuite) {
        return new Card.CardBuilder()
                .cardType(CardType.NUMBER)
                .number(number)
                .suite(cardSuite)
                .build();
    }

    public static Player buildPlayer(Card... cards) {
        Player player = new Player();
        player.getPlayerCards().addAll(Arrays.asList(cards));

        return player;
    }

    public static Game buildGame(Player... players) {
        Game game = new Game();
        game.getPlayers().addAll(Arrays.asList(players));

        return game;
    }
}
