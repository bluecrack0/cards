package com.logmein.nkovacs.cards.integration;

import com.logmein.nkovacs.cards.controller.CardController;
import com.logmein.nkovacs.cards.controller.DealController;
import com.logmein.nkovacs.cards.controller.GameController;
import com.logmein.nkovacs.cards.model.*;
import com.logmein.nkovacs.cards.repository.GameRepository;
import com.logmein.nkovacs.cards.repository.InMemoryGameRepository;
import com.logmein.nkovacs.cards.service.CardService;
import com.logmein.nkovacs.cards.service.DealService;
import com.logmein.nkovacs.cards.service.GameService;
import com.logmein.nkovacs.cards.util.DummyDataBuilder;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertEquals;

/*
Steps for end to end testing:
1. Create new empty game
2. Add new player to game
3. Add new player to game
4. Remove player from game
5. Add new cards to the game
6. Deal card to player
7. Shuffle shoe
8. Deal card to player
 */
public class GameTest {

    private GameRepository gameRepository;

    private GameController gameController;
    private CardController cardController;
    private DealController dealController;

    private GameService gameService;
    private CardService cardService;
    private DealService dealService;

    @Before
    public void setUp() {
        this.gameRepository = new InMemoryGameRepository();

        this.gameService = new GameService(this.gameRepository);
        this.gameController = new GameController(this.gameService);

        this.cardService = new CardService(this.gameRepository);
        this.cardController = new CardController(this.cardService);

        this.dealService = new DealService(this.gameRepository);
        this.dealController = new DealController(this.dealService);
    }

    @Test
    public void endToEndTest() {
        // Create Empty Game
        ResponseEntity<Game> gameResponseEntity = gameController.createEmptyGame();

        assertEquals(gameController.createEmptyGame().getStatusCode(), HttpStatus.OK);
        Game game = gameResponseEntity.getBody();

        assertThat(game, notNullValue());

        // Add two players to game
        Player player = new Player();
        Player player2 = new Player();

        ResponseEntity<Game> addPlayer = gameController.addPlayerToGame(game.getGameId(), player);
        ResponseEntity<Game> addPlayer2 = gameController.addPlayerToGame(game.getGameId(), player2);

        assertEquals(addPlayer.getStatusCode(), HttpStatus.OK);
        assertEquals(addPlayer2.getStatusCode(), HttpStatus.OK);

        Player playerWithId = addPlayer2.getBody()
                .getPlayers()
                .iterator()
                .next();

        assertThat(playerWithId.getPlayerId(), notNullValue());

        // Remove second player from game
        ResponseEntity<Game> removePlayer = gameController.removePlayerFromGame(game.getGameId(), playerWithId.getPlayerId());

        Player playerWithId2 = removePlayer.getBody()
                .getPlayers()
                .iterator()
                .next();

        assertEquals(removePlayer.getStatusCode(), HttpStatus.OK);
        assertThat(removePlayer.getBody(), notNullValue());
        assertThat(removePlayer.getBody().getPlayers(), hasSize(1));

        // Add deck to game
        Card card = DummyDataBuilder.buildCard(5, CardSuite.DIAMOND);
        Card card2 = DummyDataBuilder.buildCard(6, CardSuite.DIAMOND);
        Card card3 = DummyDataBuilder.buildCard(7, CardSuite.DIAMOND);
        Deck deck = new Deck();
        deck.getDeckCards().add(card);
        deck.getDeckCards().add(card2);
        deck.getDeckCards().add(card3);

        gameController.addDeckToGame(game.getGameId(), deck);
        Game g = gameService.getGameById(game.getGameId()).orElse(null);

        assertThat(g, notNullValue());
        assertThat(g.getDecks(), hasSize(1));
        assertThat(g.getGameDeck(), hasSize(3));

        // Deal card to player
        dealController.dealCard(game.getGameId(), playerWithId2.getPlayerId(), 1);

        Player p = gameRepository.getPlayerById(playerWithId2.getPlayerId());
        Game updated = gameRepository.getById(game.getGameId());

        assertThat(p.getPlayerCards(), hasSize(1));
        assertThat(updated.getDecks(), hasSize(1));
        assertThat(updated.getGameDeck(), hasSize(2));

        // Shuffle shoe
        dealController.shuffleShoe(game.getGameId());

        // Deal card to player
        dealController.dealCard(game.getGameId(), playerWithId2.getPlayerId(), 1);
        Player p2 = gameRepository.getPlayerById(playerWithId2.getPlayerId());
        Game updated2 = gameRepository.getById(game.getGameId());

        assertThat(p2.getPlayerCards(), hasSize(2));
        assertThat(updated2.getDecks(), hasSize(1));
        assertThat(updated2.getGameDeck(), hasSize(1));

        ResponseEntity responseEntity = gameController.deleteGame(game.getGameId());

        assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
    }
}
