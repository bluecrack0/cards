package com.logmein.nkovacs.cards.service;

import com.logmein.nkovacs.cards.model.Deck;
import com.logmein.nkovacs.cards.model.Game;
import com.logmein.nkovacs.cards.model.Player;
import com.logmein.nkovacs.cards.repository.GameRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class GameServiceTest {

    private GameService service;

    @Mock
    private GameRepository gameRepository;

    private Game game;

    @Before
    public void setUp() {
        this.gameRepository = mock(GameRepository.class);
        this.service = new GameService(gameRepository);
        doAnswer(returnsFirstArg()).when(this.gameRepository)
                .saveOrUpdate(any(Game.class));
        game = new Game();
        game.setGameId("DUMMY");
        when(gameRepository.getById(game.getGameId()))
                .thenReturn(game);
        when(gameRepository.getById("NON_EXISTING"))
                .thenReturn(null);
    }

    @Test
    public void testIfAddingPlayerToGameWorksWhenIdAndPlayerExists() {

        Player player = new Player();
        Game addedPlayerGame = service.addPlayerToGame(game.getGameId(), player)
                .orElse(null);

        assertThat(addedPlayerGame, notNullValue());
        assertThat(addedPlayerGame.getPlayers(), not(empty()));

        String id = addedPlayerGame.getPlayers()
                .stream()
                .map(Player::getPlayerId)
                .findFirst()
                .orElse("");

        assertThat(id, is("PLAYER_1"));

        Game addedPlayerGame2 = service.addPlayerToGame(game.getGameId(), player)
                .orElse(null);

        assertThat(addedPlayerGame2, notNullValue());
        assertThat(addedPlayerGame2.getPlayers(), hasSize(2));
    }

    @Test
    public void testIfAddingPlayerToGameWorksWhenIdNotExist() {

        Player player = new Player();
        Game addedPlayerGame = service.addPlayerToGame("NON_EXISTING", player)
                .orElse(null);

        assertThat(addedPlayerGame, nullValue());

        Game addedPlayerGame2 = service.addPlayerToGame(game.getGameId(), null)
                .orElse(null);

        assertThat(addedPlayerGame2, nullValue());
    }

    @Test
    public void testIfRemovingPlayerWorksIfGameAndPlayerExist() {
        Player player = new Player();
        Game addedPlayerGame = service.addPlayerToGame(game.getGameId(), player)
                .orElse(null);

        assertThat(addedPlayerGame, notNullValue());

        String addedPlayerId = addedPlayerGame.getPlayers()
                .stream()
                .map(Player::getPlayerId)
                .findFirst()
                .orElse("");

        Game removedPlayerGame = service.removePlayerFromGame(game.getGameId(), addedPlayerId)
                .orElse(null);

        assertThat(removedPlayerGame, notNullValue());
        assertThat(removedPlayerGame.getPlayers(), hasSize(0));
    }

    @Test
    public void testIfAddingDeckToGameWorks() {
        Deck deck = new Deck();

        Game addedDeckToGame = service.addDeckToGame(game.getGameId(), deck)
                .orElse(null);

        assertThat(addedDeckToGame, notNullValue());
        assertThat(addedDeckToGame.getDecks(), hasSize(1));

    }
}
