package com.logmein.nkovacs.cards.service;

import com.logmein.nkovacs.cards.model.*;
import com.logmein.nkovacs.cards.repository.GameRepository;
import com.logmein.nkovacs.cards.util.DummyDataBuilder;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CardServiceTest {

    private static Player player;
    private static Player player2;
    private static Game game;

    @Mock
    private GameRepository gameRepository;

    private CardService cardService;

    @Before
    public void setUp() {
        gameRepository = mock(GameRepository.class);
        Card.CardBuilder cardBuilder = new Card.CardBuilder();
        Card c = DummyDataBuilder.buildCard(2, CardSuite.CLUB);
        Card c2 = DummyDataBuilder.buildCard(3, CardSuite.DIAMOND);
        Card c3 = DummyDataBuilder.buildCard(4, CardSuite.CLUB);
        Card c4 = DummyDataBuilder.buildCard(2, CardSuite.HEART);

        player = DummyDataBuilder.buildPlayer(c, c2);
        player.setPlayerId("1");

        player2 = DummyDataBuilder.buildPlayer(c3, c4);
        player2.setPlayerId("2");

        Deck d = new Deck();
        d.getDeckCards().add(c);
        d.getDeckCards().add(c2);
        d.getDeckCards().add(c3);
        d.getDeckCards().add(c4);

        when(gameRepository.getPlayerById(player.getPlayerId())).thenReturn(player);
        cardService = new CardService(gameRepository);
        game = DummyDataBuilder.buildGame(player, player2);
        game.setGameId("1");
        game.getDecks().add(d);

        when(gameRepository.getById(game.getGameId())).thenReturn(game);
    }

    @Test
    public void testIfReturningPlayerCardReturnDescending() {
        List<Player> playerCardsDescending = cardService.getPlayersCardOrderDescending(game.getGameId())
                .orElse(null);

        List<Player> playerCardsDescendingNull = cardService.getPlayersCardOrderDescending(null)
                .orElse(null);

        List<Player> playerCardsDescendingNonExisting = cardService.getPlayersCardOrderDescending("?")
                .orElse(null);

        assertThat(playerCardsDescendingNull, nullValue());
        assertThat(playerCardsDescendingNonExisting, nullValue());
        assertThat(playerCardsDescending, notNullValue());
        assertThat(playerCardsDescending, hasSize(2));
        assertThat(playerCardsDescending.get(0).getPlayerId(), is("2"));
        assertThat(playerCardsDescending.get(1).getPlayerId(), is("1"));
    }

    @Test
    public void testIfGetRemainingSuitesReturnedCorrectly() {
        List<SuiteCounts> remainingSuites = cardService.getRemainingSuits(game.getGameId())
                .orElse(null);

        List<SuiteCounts> remainingSuitesNull = cardService.getRemainingSuits(null)
                .orElse(null);

        List<SuiteCounts> remainingSuitesNonExisting = cardService.getRemainingSuits("?")
                .orElse(null);

        assertThat(remainingSuitesNull, nullValue());
        assertThat(remainingSuitesNonExisting, nullValue());
        assertThat(remainingSuites, notNullValue());

        SuiteCounts club = remainingSuites.stream()
                .filter(r -> r.getCardSuite().equals(CardSuite.CLUB))
                .findAny()
                .orElse(null);

        assertThat(club, notNullValue());
        assertThat(club.getLeftCount(), is(2));

    }

    @Test
    public void testIfGetRemainingCardsReturnedAndSortedCorrectly() {
        List<Card> remainingCards = cardService.getRemainingCards(game.getGameId())
                .orElse(null);

        List<Card> remainingCards2 = cardService.getRemainingCards(null)
                .orElse(null);

        List<Card> remainingCards3 = cardService.getRemainingCards("?")
                .orElse(null);

        assertThat(remainingCards3, nullValue());
        assertThat(remainingCards2, nullValue());
        assertThat(remainingCards, notNullValue());

        assertThat(remainingCards, notNullValue());
        assertThat(remainingCards, hasSize(4));
        assertThat(remainingCards.get(0).getSuite(), is(CardSuite.CLUB));
        assertThat(remainingCards.get(0).getNumber(), is(4));
        assertThat(remainingCards.get(1).getSuite(), is(CardSuite.CLUB));
        assertThat(remainingCards.get(1).getNumber(), is(2));
        assertThat(remainingCards.get(2).getSuite(), is(CardSuite.DIAMOND));
        assertThat(remainingCards.get(2).getNumber(), is(3));

    }
}
