package com.logmein.nkovacs.cards.service;

import com.logmein.nkovacs.cards.model.*;
import com.logmein.nkovacs.cards.repository.GameRepository;
import com.logmein.nkovacs.cards.util.DummyDataBuilder;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.*;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DealServiceTest {

    private DealService dealService;

    @Mock
    private GameRepository gameRepository;

    private static Game game;
    private static Player player;

    @Before
    public void setUp() {
        gameRepository = mock(GameRepository.class);

        Card card = DummyDataBuilder.buildCard(5, CardSuite.DIAMOND);
        Card card2 = DummyDataBuilder.buildCard(6, CardSuite.CLUB);
        Card card3 = DummyDataBuilder.buildCard(7, CardSuite.HEART);
        Card card4 = DummyDataBuilder.buildCard(4, CardSuite.SPADE);
        Card card5 = DummyDataBuilder.buildCard(2, CardSuite.DIAMOND);
        Card card6 = DummyDataBuilder.buildCard(6, CardSuite.SPADE);
        Card card7 = DummyDataBuilder.buildCard(9, CardSuite.HEART);
        Card card8 = DummyDataBuilder.buildCard(1, CardSuite.SPADE);
        Card card9 = DummyDataBuilder.buildCard(11, CardSuite.SPADE);

        player = DummyDataBuilder.buildPlayer(card);
        player.setPlayerId("P1");
        game = DummyDataBuilder.buildGame(player);

        Deck deck1 = new Deck();
        deck1.getDeckCards().add(card2);
        deck1.getDeckCards().add(card3);
        deck1.getDeckCards().add(card5);
        deck1.getDeckCards().add(card7);

        Deck deck2 = new Deck();
        deck2.getDeckCards().add(card4);
        deck2.getDeckCards().add(card6);
        deck2.getDeckCards().add(card8);
        deck2.getDeckCards().add(card9);

        game.getDecks().add(deck1);
        game.getDecks().add(deck2);
        game.setGameId("G1");

        this.dealService = new DealService(gameRepository);

        when(gameRepository.getById(game.getGameId()))
                .thenReturn(game);

        when(gameRepository.saveOrUpdate(any(Game.class)))
                .then(returnsFirstArg());

    }

    @Test
    public void testIfDealingCardToPlayerRemovesCardFromDeck() {
        dealService.dealCard(game.getGameId(), player.getPlayerId(), 1);

        assertThat(game.getGameDeck(), hasSize(7));

        dealService.dealCard(game.getGameId(), player.getPlayerId(), 1);
        dealService.dealCard(game.getGameId(), player.getPlayerId(), 1);

        assertThat(game.getGameDeck(), hasSize(5));

        dealService.dealCard(game.getGameId(), player.getPlayerId(), 1);
    }

    @Test
    public void testIfShuffleActuallyShuffles() {
        List<Card> deck = new ArrayList<>(game.getGameDeck());

        dealService.shuffleShoe(game.getGameId());

        List<Card> shuffledDeck = game.getGameDeck();

        assertThat(deck, hasSize(shuffledDeck.size()));
        assertNotEquals(deck, shuffledDeck);
    }
}
