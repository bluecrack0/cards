package com.logmein.nkovacs.cards.repository;

import com.logmein.nkovacs.cards.model.Deck;
import com.logmein.nkovacs.cards.model.Game;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class InMemoryGameRepositoryTest {

    private InMemoryGameRepository gameRepository;

    @Before
    public void setUp() {
        this.gameRepository = new InMemoryGameRepository();
    }


    @Test
    public void testIfSaveWorksWhenNoEntryIsPresent() {
        Game game = new Game();
        Game savedGame = gameRepository.saveOrUpdate(game);

        assertThat(savedGame.getGameId(), notNullValue());
    }

    @Test
    public void testIfSaveWorksWhenEntryNeedsToBeUpdated() {
        Game game = new Game();
        Game savedGame = gameRepository.saveOrUpdate(game);

        assertThat(savedGame.getGameId(), notNullValue());

        Deck newDeck = new Deck();
        savedGame.getDecks().add(newDeck);
        Game updatedGame = gameRepository.saveOrUpdate(savedGame);

        assertThat(updatedGame.getDecks(), hasSize(1));
    }

    @Test
    public void testIfSaveWorksWhenNullPassed() {
        Game savedGame = gameRepository.saveOrUpdate(null);

        assertThat(savedGame, nullValue());
    }

    @Test
    public void testIfDeleteReturnsCorrectValueAfterSave() {
        Game game = new Game();
        Game savedGame = gameRepository.saveOrUpdate(game);

        assertThat(savedGame.getGameId(), notNullValue());

        boolean deleteSuccessful = gameRepository.deleteGame(savedGame.getGameId());

        assertTrue("Delete should return true if successful", deleteSuccessful);
    }

    @Test
    public void testIfDeleteReturnsCorrectValueWhenEntryNotPresent() {
        boolean deleteSuccessful = gameRepository.deleteGame("NON_EXISTING");

        assertFalse("Delete should return true if successful", deleteSuccessful);
    }

    @Test
    public void testIfGetByIdReturnsTheCorrectObjectAfterSave() {
        Game game = new Game();
        Game savedGame = gameRepository.saveOrUpdate(game);

        assertThat(savedGame.getGameId(), notNullValue());

        Game returnedGame = gameRepository.getById(savedGame.getGameId());

        assertThat(returnedGame.getGameId(), is(savedGame.getGameId()));
    }

    @Test
    public void testIfGetByIdReturnsNullIfObjectNotPresent() {
        Game returnedGame = gameRepository.getById("NON_EXISTING");

        assertThat(returnedGame, nullValue());
    }

    @Test
    public void testIfGetReturnsNullIfObjectNotPresent() {
        Game returnedGame = gameRepository.get(null);

        assertThat(returnedGame, nullValue());
    }

    @Test
    public void testIfGetReturnsCorrectObjectAfterSave() {
        Game game = new Game();
        Game savedGame = gameRepository.saveOrUpdate(game);

        assertThat(savedGame.getGameId(), notNullValue());
        Game returnedGame = gameRepository.get(savedGame);

        assertThat(returnedGame.getGameId(), is(savedGame.getGameId()));
    }
}
