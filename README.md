# Cards

## API documentation:
### /gameAPI

#### /createNewGame

Returns a Game object:
```
Example call: **POST** http://localhost:8080/gameAPI/createNewGame/
```

**Response**:
```
{
    "players": [],
    "decks": [],
    "gameId": "GAME_1",
    "gameDeck": []
}
```
#### /addDeckToGame/{gameId}

Adds a Deck to a Game, saves it to DB and returns a Game object with the added deck.

Example call: **POST** http://localhost:8080/gameAPI/addDeckToGame/GAME_1
```
{
	"deckCards": [
		{"suite":"DIAMOND","number":2,"cardType":"NUMBER"}
	]
}
```

**Response**:
```
{
    "players": [],
    "decks": [
        {
            "deckCards": [
                {
                    "suite": "DIAMOND",
                    "number": 2
                }
            ]
        }
    ],
    "gameId": "GAME_1",
    "gameDeck": [
        {
            "suite": "DIAMOND",
            "number": 2
        }
    ]
}
```
#### /addEmptyPlayerToGame/{gameId}

Adds an empty player without any cards to a Game, saves it to DB and returns a Game object with the added player.

Example call: **POST** http://localhost:8080/gameAPI/addEmptyPlayerToGame/GAME_1

**Response**:
```
{
    "players": [
        {
            "playerId": "PLAYER_1",
            "playerCards": [],
            "cardSum": 0
        }
    ],
    "decks": [
        {
            "deckCards": [
                {
                    "suite": "DIAMOND",
                    "number": 2
                }
            ]
        }
    ],
    "gameId": "GAME_1",
    "gameDeck": [
        {
            "suite": "DIAMOND",
            "number": 2
        }
    ]
}
```
#### /removePlayerFromGame/{gameId}/{playerId}

Removes a player with the given ID from the given Game. Saves the Game to DB and returns the Game object.

Example call: **POST** http://localhost:8080/gameAPI/removePlayerFromGame/GAME_1/PLAYER_1

**Response**:
```
{
    "players": [],
    "decks": [],
    "gameId": "GAME_1",
    "gameDeck": []
}
```

#### /deleteGame/{gameId}

Removes a Game from storage. Returns HTTP 200 if the deletion was successful.

Example call: **POST** http://localhost:8080/gameAPI/deleteGame/GAME_1

#### /shuffleShoe/{gameId}

Shuffles the Game's whole deck (shoe). Returns HTTP 200 if the operation was successful.

Example call: **POST** http://localhost:8080/gameAPI/shuffleShoe/GAME_1

#### /dealCard/{gameId}/{playerId}?count=X

Deals X count card to the given player in the given game. Saves the Game and Player to DB.
Returns HTTP 200 if the operation was successful. Returns HTTP 200 if the operation was successful.

Example call: **POST** http://localhost:8080/gameAPI/dealCard/GAME_1/PLAYER_1?count=1


#### /getCardsForPlayer/{playerId}

Returns the cards for the given player.

Example call: **GET** http://localhost:8080/gameAPI/getCardsForPlayer/PLAYER_1

**Response**:
```
[
    {
        "suite": "CLUB",
        "number": 4
    }
]
```

#### /getRemainingSuits/{gameId}

Returns the remaining Suites and count of them. 

Example call: **GET** http://localhost:8080/gameAPI/getRemainingSuits/GAME_1

**Response**:
```
[
    {
        "cardSuite": "HEART",
        "leftCount": 1
    },
    {
        "cardSuite": "DIAMOND",
        "leftCount": 1
    }
]
```

#### /getRemainingCards/{gameId}

Returns the remaining cards in the shoe ordered by Suite and Card number descending.

Example call: **GET** http://localhost:8080/gameAPI/getRemainingCards/GAME_1

**Response**:
```
[
    {
        "suite": "DIAMOND",
        "number": 9
    },
    {
        "suite": "DIAMOND",
        "number": 5
    },
    {
        "suite": "DIAMOND",
        "number": 2
    },
    {
        "suite": "HEART",
        "number": 3
    }
]
```

#### /getPlayersCardOrderDescending/{gameId}


Returns the players in the given game ordered by the sum of their cards in hand.

Example call: **GET** http://localhost:8080/gameAPI/getPlayersCardOrderDescending/GAME_1

**Response**:
```
[
    {
        "playerId": "PLAYER_1",
        "playerCards": [
            {
                "suite": "DIAMOND",
                "number": 5
            },
            {
                "suite": "DIAMOND",
                "number": 9
            }
        ],
        "cardSum": 14
    },
    {
        "playerId": "PLAYER_2",
        "playerCards": [
            {
                "suite": "DIAMOND",
                "number": 7
            },
            {
                "suite": "DIAMOND",
                "number": 3
            }
        ],
        "cardSum": 10
    },
    {
        "playerId": "PLAYER_3",
        "playerCards": [],
        "cardSum": 0
    }
]
```